import { all, fork } from 'redux-saga/effects';
import settingsReducer from './settings/reducer';
import phonesReducer from './phones/reducer';
import phonesEffect from './phones/effect';

export const combinedReducer = {
  phones: phonesReducer,
  settings: settingsReducer,
};
// eslint-disable-next-line func-names
export const combinedEffect = function* () {
  yield all([
    fork(phonesEffect),
  ]);
};
