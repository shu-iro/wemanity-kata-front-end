import { onError, onSuccess } from '../../../utils/axios';
import {
  PHONES_READ_ALL,
  PHONES_CREATE,
  PHONES_UPDATE,
} from './action';

const initialState = {
  error: 0,
  isFetching: false,
  phoneList: [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PHONES_READ_ALL:
    case PHONES_CREATE:
    case PHONES_UPDATE:
      return {
        ...state,
        error: 0,
        isFetching: true,
      };
    case onSuccess(PHONES_READ_ALL):
      return {
        ...state,
        isFetching: false,
        phoneList: action.data,
      };
    case onSuccess(PHONES_CREATE):
    case onSuccess(PHONES_UPDATE):
      // Don't store the updated/created phone
      // because we prefer retrieve all phone to keep application up to date
      // If time implement simple short polling
      return {
        ...state,
        isFetching: false,
      };
    case onError(PHONES_READ_ALL):
      return {
        ...state,
        error: action.error,
        isFetching: false,
        phoneList: [],
      };
    case onError(PHONES_CREATE):
    case onError(PHONES_UPDATE):
      return {
        ...state,
        error: action.error,
        isFetching: false,
      };
    default:
      return state;
  }
};

export default reducer;
