import ENDPOINTS from '../../../constants/endpoint';

export const PHONES_READ_ALL = 'PHONES_READ_ALL';
export const PHONES_CREATE = 'PHONES_CREATE';
export const PHONES_UPDATE = 'PHONES_UPDATE';

export const phonesReadAll = () => ({
  request: {
    method: 'GET',
    url: ENDPOINTS.GET_PHONE_READ,
  },
  type: PHONES_READ_ALL,
});

export const phonesCreate = (phone) => ({
  request: {
    data: phone,
    method: 'PUT',
    url: ENDPOINTS.PUT_PHONE_CREATE,
  },
  type: PHONES_CREATE,
});

export const phonesUpdate = (phoneOID, phone) => ({
  request: {
    data: phone,
    method: 'POST',
    url: ENDPOINTS.POST_PHONE_UPDATE.replace(':phoneOID', phoneOID),
  },
  type: PHONES_UPDATE,
});
