import { all, takeLatest } from 'redux-saga/effects';
import { callAPI } from '../../../utils/axios';
import {
  PHONES_UPDATE,
  PHONES_CREATE,
  PHONES_READ_ALL,
} from './action';

// callAPI automatically send a request to API (or fake API) based on action { request: ... }
// And dispatch MY_ACTION_SUCCESS or _ERROR based on response
export default function* () {
  yield all([
    takeLatest(PHONES_UPDATE, callAPI),
    takeLatest(PHONES_CREATE, callAPI),
    takeLatest(PHONES_READ_ALL, callAPI),
  ]);
}
