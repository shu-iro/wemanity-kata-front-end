import LANGUAGES from '../../../constants/language';
import {
  SETTINGS_SET_LANGUAGE,
  SETTINGS_ACCEPT_GDPR,
} from './action';

const initialState = {
  isAgreeGDPR: false,
  language: LANGUAGES.english,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SETTINGS_SET_LANGUAGE:
      return {
        ...state,
        language: action.data,
      };
    case SETTINGS_ACCEPT_GDPR:
      return {
        ...state,
        isAgreeGDPR: true,
      };
    default:
      return state;
  }
};

export default reducer;
