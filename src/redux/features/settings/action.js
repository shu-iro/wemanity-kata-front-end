export const SETTINGS_SET_LANGUAGE = 'SETTINGS_SET_LANGUAGE';
export const SETTINGS_ACCEPT_GDPR = 'SETTINGS_ACCEPT_GDPR';

export const settingsSetLanguage = (language) => ({
  data: language,
  type: SETTINGS_SET_LANGUAGE,
});
export const settingsAcceptGDPR = () => ({
  type: SETTINGS_ACCEPT_GDPR,
});
