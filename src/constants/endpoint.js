export default {
  GET_PHONE_READ: `${process.env.REACT_APP_BASE_URL}/phone`,
  PHONE: `${process.env.REACT_APP_BASE_URL}/phone`,
  POST_PHONE_UPDATE: `${process.env.REACT_APP_BASE_URL}/phone/:phoneOID`,
  PUT_PHONE_CREATE: `${process.env.REACT_APP_BASE_URL}/phone`,
};
