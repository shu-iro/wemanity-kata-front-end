import React, { useState } from 'react';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core';
import themeO2B from '../../../theme/o2b';
import extProps from './propTypes';

/*
 *
 * Material-Design theme wrapper loaded with O2B theme
 *
 */
const ThemeWrapper = ({ classes, children }) => {
  const [theme] = useState(createMuiTheme(themeO2B(process.env.REACT_APP_THEME_COLOR,
    process.env.REACT_APP_THEME_MODE)));

  return (
    <MuiThemeProvider theme={theme}>
      <div className={`${classes.root}`}>
        { children }
      </div>
    </MuiThemeProvider>
  );
};

ThemeWrapper.propTypes = extProps;

export default ThemeWrapper;
