import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import PhoneBook from '../../PhoneBook';

/*
 *
 * Router with all route available
 *
 */
const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route component={PhoneBook} path="/" />
    </Switch>
  </BrowserRouter>
);

export default Router;
