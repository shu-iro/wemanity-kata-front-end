import React, { useState } from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import ThemeWrapper from './ThemeWrapper';
import Router from './Router';

/*
 *
 * Root of the app, initialize theme, reset css(CssBaseline) and initialize router for navigation
 *
 */
const App = () => {
  // eslint-disable-next-line no-unused-vars
  const [isLoading, setIsLoading] = useState(true);

  return (
    <ThemeWrapper>
      <CssBaseline />
      <Router />
    </ThemeWrapper>
  );
};

export default App;
