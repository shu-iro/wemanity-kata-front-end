import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux';
import { settingsSetLanguage, settingsAcceptGDPR } from '../../redux/features/settings/action';
import { phonesCreate, phonesReadAll, phonesUpdate } from '../../redux/features/phones/action';
import styles from './styles';
import PhoneBook from './PhoneBook';

const mapStateToProps = (state) => ({
  error: state.phones.error,
  isAgreeGDPR: state.settings.isAgreeGDPR,
  isFetching: state.phones.isFetching,
  language: state.settings.language,
  phoneList: state.phones.phoneList,
});

const mapDispatchToProps = (dispatch) => ({
  acceptGDPR: () => dispatch(settingsAcceptGDPR()),
  createPhone: (phone) => dispatch(phonesCreate(phone)),
  readAllPhone: () => dispatch(phonesReadAll()),
  setLanguage: (language) => dispatch(settingsSetLanguage(language)),
  updatePhone: (phoneOID, phone) => dispatch(phonesUpdate(phoneOID, phone)),
});

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(PhoneBook));
