export default (theme) => ({
  cardContentPhoneBook: {
    [theme.breakpoints.down('xs')]: {
      height: 350,
    },
    '&::-webkit-scrollbar': {
      width: 8,
    },
    '&::-webkit-scrollbar-thumb': {
      backgroundColor: 'rgba(0,0,0,0)',
      borderRadius: 12,
    },
    '&:hover': {
      '&::-webkit-scrollbar-thumb': {
        backgroundColor: 'rgba(0,0,0,0.3)',
        border: '1px solid rgba(255,255,255,0.4)',
      },
    },
    height: 400,
    overflowY: 'scroll',
  },
  gridItemCreate: {
    textAlign: 'right',
  },
  gridRoot: {
    flexGrow: 1,
  },
  iconBlack: {
    '& > path': {
      fill: theme.palette.primary.dark,
    },
  },
  input: {
    '& .MuiOutlinedInput-root': {
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary.dark,
      },
    },
    '& label.Mui-focused': {
      color: theme.palette.primary.dark,
    },
    animationDuration: '.3s',
    borderColor: theme.palette.primary.dark,
    color: theme.palette.primary.dark,
    marginBottom: 0,
    marginTop: 0,
    paddingRight: theme.spacing(2),
  },
  root: {
    backgroundColor: theme.palette.secondary.dark,
    color: theme.palette.secondary.contrastText,
    display: 'flex',
    fontFamily: 'Open Sans',
    fontSize: '1rem',
    fontStyle: 'normal',
    fontWeight: 400,
    height: '100vh',
    lineHeight: 2,
    margin: 0,
    overflowX: 'hidden',
    overflowY: 'hidden',
    padding: 0,
  },
});
