import React from 'react';
import {
  Grid, Typography,
} from '@material-ui/core';
import extProps from './propTypes';
import { PhoneCard, PhoneCardLoading } from '../../../components';

const PhoneBookRead = ({
  classes, isFetching, onUpdatePhone, phoneList, messages,
}) => (
  <Grid
    container
    spacing={1}
    className={(!isFetching && !phoneList.length) && classes.containerPhoneCard}
    alignItems={!isFetching && !phoneList.length ? 'center' : 'stretch'}
  >
    {
      // Show placeholder phone card during loading
      isFetching ? [
        <Grid item xs={12}><PhoneCardLoading /></Grid>,
        <Grid item xs={12}><PhoneCardLoading /></Grid>,
        <Grid item xs={12}><PhoneCardLoading /></Grid>,
        <Grid item xs={12}><PhoneCardLoading /></Grid>,
      ] : <></>
    }
    {
      // Show list of phone card if is not fetching and have phone in list
      !isFetching && phoneList.length ? phoneList.map((phone) => (
        <Grid item xs={12} className="animated slideInDown faster">
          <PhoneCard
            messages={messages}
            name={`${phone.lastName} ${phone.firstName}`}
            phoneNumber={phone.phoneNumber}
            onEdit={() => onUpdatePhone(phone.oid)}
          />
        </Grid>
      )) : <></>
    }
    {
      // Show a message to tell PhoneBook is empty if not fetching and no phone
      !isFetching && !phoneList.length ? (
        <Grid item xs={12} className="animated fadeIn faster">
          <Typography variant="h3" align="center">{messages['PhoneBook is empty.']}</Typography>
        </Grid>
      ) : <></>
    }
  </Grid>
);

PhoneBookRead.propTypes = extProps;

export default PhoneBookRead;
