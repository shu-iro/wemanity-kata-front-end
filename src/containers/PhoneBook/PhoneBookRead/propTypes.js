import { PropTypes } from 'prop-types';

import Phone from '../../../types/Phone';

const propTypes = {
  classes: PropTypes.object.isRequired,
  isFetching: PropTypes.bool.isRequired,
  messages: PropTypes.object.isRequired,
  onUpdatePhone: PropTypes.func.isRequired,
  phoneList: PropTypes.arrayOf(PropTypes.shape(Phone)).isRequired,
};

export default propTypes;
