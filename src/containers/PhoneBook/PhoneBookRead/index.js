import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import PhoneBookRead from './PhoneBookRead';

export default withStyles(styles)(PhoneBookRead);
