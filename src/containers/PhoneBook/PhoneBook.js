import React, { useState } from 'react';
import {
  Grid,
  Card,
  CardHeader,
  IconButton,
  CardContent,
  CardActions,
  TextField, ClickAwayListener,
} from '@material-ui/core';
import {
  Phone, Add, Search,
} from '@material-ui/icons';

import extProps from './propTypes';
import languageProvider from '../../translations';
import PhoneBookRead from './PhoneBookRead';
import {
  TranslationButton, PhoneCardForm, DialogGDPR, SnackbarWrapper,
} from '../../components';
import {
  useCreatePhoneErrorSuccess,
  useFilterPhoneList,
  useFirstTimeReadPhone,
  useReadPhoneErrorSuccess,
  useUpdatePhoneErrorSuccess,
} from './logic';

const PhoneBook = ({
  classes, language, setLanguage, isAgreeGDPR, acceptGDPR,
  phoneList, readAllPhone, isFetching, error, createPhone, updatePhone,
}) => {
  // Read, search, phone list
  const [isFirstRead, setIsFirstRead] = useState(true);
  const [isFetchingRead, setIsFetchingRead] = useState(false);
  const [isShowSearch, setIsShowSearch] = useState(false);
  const [inputSearch, setInputSearch] = useState('');
  const [phoneListFilter, setPhoneListFilter] = useState([]);
  const [isShowNotifRead, setIsShowNotifRead] = useState(false);

  // Create
  const [isFetchingCreate, setIsFetchingCreate] = useState(false);
  const [isShowCreate, setIsShowCreate] = useState(false);
  const [isShowNotifCreate, setIsShowNotifCreate] = useState(false);

  // Update
  const [isFetchingUpdate, setIsFetchingUpdate] = useState(false);
  const [phoneToUpdate, setPhoneToUpdate] = useState(null);
  const [isShowNotifUpdate, setIsShowNotifUpdate] = useState(false);

  useFirstTimeReadPhone(isFirstRead, isFetchingRead, readAllPhone,
    setIsFirstRead, setIsFetchingRead);

  useReadPhoneErrorSuccess(isFetching, isFetchingRead, setIsFetchingRead, setIsShowNotifRead);

  useFilterPhoneList(phoneList, setPhoneListFilter, inputSearch);

  useCreatePhoneErrorSuccess(isFetching, isFetchingCreate, setIsFetchingCreate,
    error, readAllPhone, setIsFetchingRead, setIsShowCreate, setIsShowNotifCreate);

  useUpdatePhoneErrorSuccess(isFetching, isFetchingUpdate, setIsFetchingUpdate,
    error, readAllPhone, setIsFetchingRead, setPhoneToUpdate, setIsShowNotifUpdate);

  // When switch to search/create/update disable other show and reset search input
  const showSearch = () => {
    setIsShowSearch(true);
    setIsShowCreate(false);
    setPhoneToUpdate(null);
    setInputSearch('');
  };
  const showCreate = () => {
    setIsShowCreate(true);
    setPhoneToUpdate(null);
    setIsShowSearch(false);
    setInputSearch('');
  };
  const showUpdate = (phone) => {
    setPhoneToUpdate(phone);
    setIsShowCreate(false);
    setIsShowSearch(false);
    setInputSearch('');
  };

  const onCreatePhone = (phone) => {
    if (!phone) return;
    setIsFetchingCreate(true);
    createPhone(phone); // PUT /phone
  };
  const onUpdatePhone = (phone) => {
    if (!phone) return;
    setIsFetchingUpdate(true);
    updatePhone(phone.oid, phone); // POST /phone/:oid
  };

  // Give access to translated message
  const messages = languageProvider[language];

  return (
    <div className={classes.root}>
      <Grid className={classes.gridRoot} container alignContent="center" alignItems="center">
        <Grid item zeroMinWidth lg={3} md={2} xs={0} />
        <Grid item lg={6} md={8} xs={12}>
          <Card>
            <CardHeader
              avatar={<Phone className={classes.iconBlack} />}
              action={<TranslationButton onSetLanguage={setLanguage} />}
              title={messages['Manage your PhoneBook']}
              subheader="Wemanity kata"
            />
            <CardContent className={classes.cardContentPhoneBook}>
              {
                // Show list of phone if click on search bar or if we are not in create/update
                isShowSearch || (!isShowCreate && !phoneToUpdate) ? (
                  <PhoneBookRead
                    messages={messages}
                    isFetching={isFetchingRead}
                    phoneList={phoneListFilter}
                    onUpdatePhone={(oid) => showUpdate(phoneList
                      .filter((p) => p.oid === oid)[0])}
                  />
                ) : <></>
              }
              {
                isShowCreate ? (
                  <PhoneCardForm
                    messages={messages}
                    onConfirm={isFetching ? () => {} : onCreatePhone}
                    onCancel={() => setIsShowCreate(false)}
                  />
                ) : <></>
              }
              {
                phoneToUpdate ? (
                  <PhoneCardForm
                    messages={messages}
                    phoneToUpdate={isFetching ? () => {} : phoneToUpdate}
                    onConfirm={(p) => onUpdatePhone({ ...phoneToUpdate, ...p })}
                    onCancel={() => setPhoneToUpdate(null)}
                  />
                ) : <></>
              }
            </CardContent>
            <CardActions>
              <Grid container alignItems="center">
                {
                  // By default show small search button
                  !isShowSearch ? (
                    <>
                      <Grid item xs={2} md={1}>
                        <IconButton onClick={showSearch}>
                          <Search className={classes.iconBlack} />
                        </IconButton>
                      </Grid>
                      <Grid item xs={2} md={1}>
                        <IconButton onClick={showCreate}>
                          <Add className={classes.iconBlack} />
                        </IconButton>
                      </Grid>
                    </>
                  ) : <></>
                }
                {
                  // Show search bar if click on search button
                  // Hide if click away
                  isShowSearch ? (
                    <>
                      <Grid item xs={10} md={11}>
                        <ClickAwayListener onClickAway={() => setIsShowSearch(false)}>
                          <TextField
                            value={inputSearch}
                            className={`${classes.input} animated slideInLeft`}
                            label={messages.Search}
                            fullWidth
                            variant="outlined"
                            margin="normal"
                            onChange={(e) => setInputSearch(e.target.value)}
                          />
                        </ClickAwayListener>
                      </Grid>
                      <Grid item xs={2} md={1}>
                        <IconButton onClick={() => setIsShowCreate(true)}>
                          <Add className={classes.iconBlack} />
                        </IconButton>
                      </Grid>
                    </>
                  ) : <></>
                }
              </Grid>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
      <DialogGDPR
        messages={languageProvider[language]}
        isShowGDPR={!isAgreeGDPR}
        onHideGDPR={acceptGDPR}
      />

      <SnackbarWrapper
        message={messages['Loading PhoneBook...']}
        bgInfo
        isShow={isFetchingRead}
        onClose={() => {}}
      />
      <SnackbarWrapper
        message={error ? `${messages['Failed to load PhoneBook...('] + error})` : messages['PhoneBook loaded !']}
        bgError={!!error}
        bgSuccess={!error}
        isShow={isShowNotifRead}
        onClose={() => setIsShowNotifRead(false)}
        autoHideDuration={2000}
      />

      <SnackbarWrapper
        message={messages['Save new phone...']}
        bgInfo
        isShow={isFetchingCreate}
        onClose={() => {}}
      />
      <SnackbarWrapper
        message={error ? `${messages['Failed to save phone...('] + error})` : messages['New phone saved !']}
        bgError={!!error}
        bgSuccess={!error}
        isShow={isShowNotifCreate}
        onClose={() => setIsShowNotifCreate(false)}
        autoHideDuration={2000}
      />

      <SnackbarWrapper
        message={messages['Update phone...']}
        bgInfo
        isShow={isFetchingUpdate}
        onClose={() => {}}
      />
      <SnackbarWrapper
        message={error ? `${messages['Failed to update phone...('] + error})` : messages['Phone updated !']}
        bgError={!!error}
        bgSuccess={!error}
        isShow={isShowNotifUpdate}
        onClose={() => setIsShowNotifUpdate(false)}
        autoHideDuration={2000}
      />
    </div>
  );
};

PhoneBook.propTypes = extProps;

export default PhoneBook;
