import { useEffect } from 'react';

export const useFirstTimeReadPhone = (isFirstRead, isFetchingRead, readAllPhone,
  setIsFirstRead, setIsFetchingRead) => useEffect(() => {
  if (isFirstRead && !isFetchingRead) {
    // First time we load the component, try to read all phone
    readAllPhone();
    setIsFirstRead(false);
    setIsFetchingRead(true);
  }
}, [isFirstRead, isFetchingRead, readAllPhone, setIsFirstRead, setIsFetchingRead]);

export const useReadPhoneErrorSuccess = (isFetching, isFetchingRead,
  setIsFetchingRead, setIsShowNotifRead) => useEffect(() => {
  if (!isFetching && isFetchingRead) {
    // Retrieve phone list done, show snackbar notif success/error
    setIsFetchingRead(false);
    setIsShowNotifRead(true);
  }
}, [isFetching, isFetchingRead, setIsFetchingRead, setIsShowNotifRead]);

export const useFilterPhoneList = (phoneList, setPhoneListFilter, inputSearch) => useEffect(() => {
  // Filter new phone list with new search term
  setPhoneListFilter(phoneList
    .filter((phone) => (`${phone.phoneNumber} ${phone.lastName} ${phone.firstName}`).toLocaleLowerCase()
      .includes(inputSearch.toLocaleLowerCase().trim())));
}, [phoneList, setPhoneListFilter, inputSearch]);

export const useCreatePhoneErrorSuccess = (isFetching, isFetchingCreate, setIsFetchingCreate, error,
  readAllPhone, setIsFetchingRead, setIsShowCreate, setIsShowNotifCreate) => useEffect(() => {
  if (!isFetching && isFetchingCreate) {
    if (!error) {
      // Successfully create phone, retrieve phone list
      readAllPhone();
      setIsFetchingRead(true);
      setIsShowCreate(false);
    }
    // Create phone done, show snackbar notif success/error
    setIsFetchingCreate(false);
    setIsShowNotifCreate(true);
  }
}, [isFetching, isFetchingCreate, setIsFetchingCreate, error,
  readAllPhone, setIsFetchingRead, setIsShowCreate, setIsShowNotifCreate]);

export const useUpdatePhoneErrorSuccess = (isFetching, isFetchingUpdate, setIsFetchingUpdate, error,
  readAllPhone, setIsFetchingRead, setPhoneToUpdate, setIsShowNotifUpdate) => useEffect(() => {
  if (!isFetching && isFetchingUpdate) {
    if (!error) {
      // Successfully update phone, retrieve phone list
      readAllPhone();
      setIsFetchingRead(true);
      setPhoneToUpdate(null);
    }
    // Update phone done, show snackbar notif success/error
    setIsFetchingUpdate(false);
    setIsShowNotifUpdate(true);
  }
}, [isFetching, isFetchingUpdate, setIsFetchingUpdate, error,
  readAllPhone, setIsFetchingRead, setPhoneToUpdate, setIsShowNotifUpdate]);
