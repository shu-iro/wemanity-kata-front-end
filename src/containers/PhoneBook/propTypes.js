import { PropTypes } from 'prop-types';

import Phone from '../../types/Phone';

const propTypes = {
  acceptGDPR: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
  createPhone: PropTypes.func.isRequired,
  error: PropTypes.number.isRequired,
  isAgreeGDPR: PropTypes.bool.isRequired,
  isFetching: PropTypes.bool.isRequired,
  language: PropTypes.string.isRequired,
  phoneList: PropTypes.arrayOf(PropTypes.shape(Phone)).isRequired,
  readAllPhone: PropTypes.func.isRequired,
  setLanguage: PropTypes.func.isRequired,
  updatePhone: PropTypes.func.isRequired,
};

export default propTypes;
