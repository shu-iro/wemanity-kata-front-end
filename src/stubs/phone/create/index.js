export default (state, phone) => {
  const newState = state;
  newState.phoneList = [...newState.phoneList,
    {
      ...phone,
      oid: `5dc579b978a9c82368cb8f0${state.phoneList.length}`,
    },
  ];

  return [newState, { data: newState.phoneList[-1], status: 200 }];
};
