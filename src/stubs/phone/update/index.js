export default (state, oid, phone) => {
  const newState = state;
  const phoneToUpdate = newState.phoneList.filter((p) => p.oid === oid)[0];
  phoneToUpdate.lastName = phone.lastName;
  phoneToUpdate.firstName = phone.firstName;
  phoneToUpdate.phoneNumber = phone.phoneNumber;

  return [newState, { data: phoneToUpdate, status: 200 }];
};
