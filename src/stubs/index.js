import ENDPOINTS from '../constants/endpoint';
import phoneReadAll from './phone/read';
import phoneCreate from './phone/create';
import phoneUpdate from './phone/update';

let state = { phoneList: [] };

// eslint-disable-next-line no-unused-vars
export default (method, url, data = {}) => {
  if (url.includes(ENDPOINTS.PHONE)) {
    if (method === 'GET') {
      const [newState, response] = phoneReadAll(state, data);
      state = newState;
      return response;
    }
    if (method === 'PUT') {
      const [newState, response] = phoneCreate(state, data);
      state = newState;
      return response;
    }
    if (method === 'POST') {
      const [newState, response] = phoneUpdate(state, url.split('/')[2], data);
      state = newState;
      return response;
    }
  }

  return {
    error: {
      code: 404,
      message: 'Endpoint not found...',
    },
  };
};
