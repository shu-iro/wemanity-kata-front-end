import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import PhoneCard from './PhoneCard';

export default withStyles(styles)(PhoneCard);
