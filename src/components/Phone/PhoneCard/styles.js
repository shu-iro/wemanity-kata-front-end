export default (theme) => ({
  actionPhoneCard: {
    [theme.breakpoints.down('xs')]: {
      display: 'grid',
    },
    display: 'block',
  },
  iconBlack: {
    '& > path': {
      fill: theme.palette.primary.dark,
    },
  },
});
