import React, { useState } from 'react';
import {
  Card, CardHeader, ClickAwayListener, IconButton, Tooltip,
} from '@material-ui/core';
import { Edit, FileCopy } from '@material-ui/icons';
import copy from 'clipboard-copy';
import extProps from './propTypes';

const PhoneCard = ({
  classes, name, phoneNumber, onEdit, messages,
}) => {
  const [isShowCopy, setIsShowCopy] = useState(false);

  return (
    <Card>
      <CardHeader
        title={name}
        subheader={phoneNumber}
        action={(
          <div className={classes.actionPhoneCard}>
            <ClickAwayListener onClickAway={() => setIsShowCopy(false)}>
              <Tooltip
                PopperProps={{ disablePortal: true }}
                onClose={() => setIsShowCopy(false)}
                open={isShowCopy}
                disableFocusListener
                disableHoverListener
                disableTouchListener
                title={messages['Number copied !']}
              >
                <IconButton
                  onClick={() => { copy(phoneNumber); setIsShowCopy(true); }}
                  className={isShowCopy && 'animated rubberBand'}
                >
                  <FileCopy className={classes.iconBlack} />
                </IconButton>
              </Tooltip>
            </ClickAwayListener>
            <IconButton onClick={onEdit}>
              <Edit className={classes.iconBlack} />
            </IconButton>
          </div>
        )}
      />
    </Card>
  );
};

PhoneCard.propTypes = extProps;

export default PhoneCard;
