import { PropTypes } from 'prop-types';
import Phone from '../../../types/Phone';

export const propTypes = {
  classes: PropTypes.object.isRequired,
  messages: PropTypes.object.isRequired,
  onCancel: PropTypes.func.isRequired,
  onConfirm: PropTypes.func.isRequired,
  phoneToUpdate: PropTypes.shape(Phone),
};

export const defaultProps = {
  phoneToUpdate: null,
};

export default propTypes;
