import React, { useEffect, useState } from 'react';
import {
  Card, CardContent, CardHeader, IconButton, Grid, TextField, Tooltip,
} from '@material-ui/core';
import { Done, Clear, Report } from '@material-ui/icons';
import { propTypes as extProps, defaultProps as extDefault } from './propTypes';

const PhoneCardForm = ({
  classes, onCancel, onConfirm, phoneToUpdate, messages,
}) => {
  const [inputFirstName, setInputFirstName] = useState(phoneToUpdate ? phoneToUpdate.firstName : '');
  const [inputLastName, setInputLastName] = useState(phoneToUpdate ? phoneToUpdate.lastName : '');
  const [inputPhoneNumber, setInputPhoneNumber] = useState(phoneToUpdate ? phoneToUpdate.phoneNumber : '');
  // Valid by default if phone to update
  const [isValid, setIsValid] = useState(!!phoneToUpdate);

  const checkFirstName = (ifn) => !!ifn;
  const checkLastName = (iln) => !!iln;
  const checkPhoneNumber = (ipn) => /^([+][0-9]+[\s][0-9]+[\s][0-9]{6,})$/.test(ipn);
  const generateMessage = (ifn, iln, ipn) => {
    if (!checkFirstName(ifn)) return messages['Please provide first name.'];
    if (!checkLastName(iln)) return messages['Please provide last name.'];
    if (!checkPhoneNumber(ipn)) return messages['Please provide correct phone number.'];
    return '';
  };

  useEffect(() => {
    setIsValid(checkFirstName(inputFirstName) && checkLastName(inputLastName)
      && checkPhoneNumber(inputPhoneNumber));
  }, [inputFirstName, inputLastName, inputPhoneNumber, setIsValid]);

  return (
    <Card className="animated slideInRight faster">
      <CardHeader
        title={phoneToUpdate ? messages['Update an entry'] : messages['Create an entry']}
        subheader={messages['All fields are mandatory, phone number should be in the following format +39 02 1234567']}
        action={(
          <div className={classes.actionPhoneCard}>
            <IconButton onClick={onCancel}>
              <Clear className={classes.iconBlack} />
            </IconButton>
            {
              isValid ? (
                <IconButton
                  className="animated rubberBand"
                  onClick={() => onConfirm({
                    firstName: inputFirstName,
                    lastName: inputLastName,
                    phoneNumber: inputPhoneNumber,
                  })}
                >
                  <Done className={classes.iconGreen} />
                </IconButton>

              ) : (
                <Tooltip title={generateMessage(inputFirstName, inputLastName, inputPhoneNumber)}>
                  <IconButton disableRipple>
                    <Report className={classes.iconRed} />
                  </IconButton>
                </Tooltip>
              )
            }
          </div>
        )}
      />
      <CardContent>
        <Grid container>
          <Grid item xs={6}>
            <TextField
              value={inputLastName}
              className={classes.input}
              label={messages['Last name']}
              fullWidth
              variant="outlined"
              margin="normal"
              onChange={(e) => setInputLastName(e.target.value)}
            />
          </Grid>
          <Grid item xs={6}>
            <TextField
              value={inputFirstName}
              className={classes.input}
              label={messages['First name']}
              fullWidth
              variant="outlined"
              margin="normal"
              onChange={(e) => setInputFirstName(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
              value={inputPhoneNumber}
              className={classes.input}
              label={messages['Phone number']}
              fullWidth
              variant="outlined"
              margin="normal"
              onChange={(e) => setInputPhoneNumber(e.target.value)}
            />
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  );
};

PhoneCardForm.propTypes = extProps;
PhoneCardForm.defaultProps = extDefault;

export default PhoneCardForm;
