import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import PhoneCardForm from './PhoneCardForm';

export default withStyles(styles)(PhoneCardForm);
