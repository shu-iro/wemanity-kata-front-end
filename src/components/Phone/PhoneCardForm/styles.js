import { red, green } from '@material-ui/core/colors';

export default (theme) => ({
  actionPhoneCard: {
    [theme.breakpoints.down('xs')]: {
      display: 'grid',
    },
    display: 'block',
  },
  iconBlack: {
    '& > path': {
      fill: theme.palette.primary.dark,
    },
  },
  iconGreen: {
    '& > path': {
      fill: green[500],
    },
  },
  iconRed: {
    '& > path': {
      fill: red[500],
    },
  },
  input: {
    '& .MuiOutlinedInput-root': {
      '&.Mui-focused fieldset': {
        borderColor: theme.palette.primary.dark,
      },
    },
    '& label.Mui-focused': {
      color: theme.palette.primary.dark,
    },
    borderColor: theme.palette.primary.dark,
    color: theme.palette.primary.dark,
    marginBottom: theme.spacing(1),
    marginTop: theme.spacing(1),
    paddingRight: theme.spacing(2),
  },
});
