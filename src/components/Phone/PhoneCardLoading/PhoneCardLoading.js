import React from 'react';
import {
  Card, CardHeader, IconButton,
} from '@material-ui/core';
import { Edit, FileCopy } from '@material-ui/icons';
import extProps from './propTypes';

const PhoneCardLoading = ({ classes }) => (
  <Card className={classes.cardLoading}>
    <CardHeader
      title={(<div className={classes.titleLoading} />)}
      subheader={(<div className={classes.subheaderLoading} />)}
      action={(
        <div className={classes.actionPhoneCard}>
          <IconButton disabled>
            <FileCopy className={classes.iconFake} />
          </IconButton>
          <IconButton disabled>
            <Edit className={classes.iconFake} />
          </IconButton>
        </div>
      )}
    />
  </Card>
);

PhoneCardLoading.propTypes = extProps;

export default PhoneCardLoading;
