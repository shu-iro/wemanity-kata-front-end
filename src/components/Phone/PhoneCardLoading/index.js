import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import PhoneCardLoading from './PhoneCardLoading';

export default withStyles(styles)(PhoneCardLoading);
