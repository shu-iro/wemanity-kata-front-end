import { grey } from '@material-ui/core/colors';

export default (theme) => ({
  actionPhoneCard: {
    [theme.breakpoints.down('xs')]: {
      display: 'grid',
    },
    display: 'block',
  },
  cardLoading: {
    backgroundColor: grey[100],
  },
  iconFake: {
    '& > path': {
      fill: grey[200],
    },
    backgroundColor: grey[200],
  },
  subheaderLoading: {
    backgroundColor: grey[200],
    height: '1.2rem',
    marginTop: 3,
    width: '30%',
  },
  titleLoading: {
    backgroundColor: grey[200],
    height: '1.7rem',
    width: '50%',
  },
});
