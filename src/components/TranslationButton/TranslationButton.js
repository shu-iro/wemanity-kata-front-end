/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import {
  Popper, Grow, Paper, ClickAwayListener, MenuList, MenuItem, IconButton,
} from '@material-ui/core';
import { Translate } from '@material-ui/icons';
import extProps from './propTypes';

const TranslationButton = ({
  classes, onSetLanguage,
}) => {
  const [isOpen, setIsOpen] = useState(false);
  const [useRef] = useState(React.createRef());

  return (
    <>
      <IconButton
        onClick={() => setIsOpen(!isOpen)}
        aria-owns={isOpen ? 'menu-list-grow' : undefined}
        aria-haspopup="true"
        ref={useRef}
        aria-label="settings"
      >
        <Translate className={classes.iconBlack} />
      </IconButton>
      <Popper
        open={isOpen}
        anchorEl={useRef.current}
        transition
        disablePortal
        className={classes.translatePopper}
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom',
            }}
          >
            <Paper id="menu-list-grow" className={classes.translatePaper}>
              <ClickAwayListener onClickAway={() => setIsOpen(false)}>
                <MenuList>
                  <MenuItem
                    className={`${classes.translateItem} ${classes.borderBottom}`}
                    onClick={() => {
                      onSetLanguage('english');
                      setIsOpen(false);
                    }}
                  >
                  English
                  </MenuItem>
                  <MenuItem
                    className={`${classes.translateItem} ${classes.borderBottom}`}
                    onClick={() => {
                      onSetLanguage('french');
                      setIsOpen(false);
                    }}
                  >
                  Français
                  </MenuItem>
                  <MenuItem disabled>
                  Nederlands
                  </MenuItem>
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </>
  );
};

TranslationButton.propTypes = extProps;

export default TranslationButton;
