import { PropTypes } from 'prop-types';

const propTypes = {
  classes: PropTypes.object.isRequired,
  onSetLanguage: PropTypes.func.isRequired,
};

export default propTypes;
