export default (theme) => ({
  iconBlack: {
    '& > path': {
      fill: theme.palette.primary.dark,
    },
  },
  translateItem: {
    '&:hover': {
      color: theme.palette.primary.contrastText,
    },
    transition: 'color 0.2s ease-out 0s',
  },
  translatePaper: {
    backgroundColor: theme.palette.primary.dark,
    color: '#fff',
    zIndex: 20,
  },
  translatePopper: {
    marginBottom: theme.spacing(0.5),
    marginTop: theme.spacing(0.5),
    zIndex: 20,
  },
});
