import DialogGDPR from './DialogGDPR';
import TranslationButton from './TranslationButton';
import SnackbarWrapper from './SnackbarWrapper';

import PhoneCard from './Phone/PhoneCard';
import PhoneCardLoading from './Phone/PhoneCardLoading';
import PhoneCardForm from './Phone/PhoneCardForm';

// eslint-disable-next-line import/prefer-default-export
export {
  DialogGDPR,
  TranslationButton,
  SnackbarWrapper,

  PhoneCard,
  PhoneCardLoading,
  PhoneCardForm,
};
