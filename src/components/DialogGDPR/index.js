import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import DialogGDPR from './DialogGDPR';

export default withStyles(styles)(DialogGDPR);
