export default (theme) => ({
  dialogAction: {
    color: theme.palette.primary.dark,
  },
  dialogContent: {
    '& h5': {
      marginBottom: theme.spacing(3),
    },
    '& h6': {
      marginBottom: theme.spacing(2),
    },
    '& p': {
      marginBottom: theme.spacing(1),
    },
  },
  info: {
    color: theme.palette.primary.contrastText,
    paddingBottom: theme.spacing(0.5),
    paddingLeft: theme.spacing(1),
    paddingTop: theme.spacing(0.5),
  },
  snackbar: {
    '& > div': {
      borderRadius: theme.spacing(1),
    },
    width: '95%',
  },
});
