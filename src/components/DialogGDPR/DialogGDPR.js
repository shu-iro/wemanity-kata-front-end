import React, { useState } from 'react';
import {
  Snackbar, Button, IconButton, Dialog, DialogContent, Typography, DialogActions,
} from '@material-ui/core';
import { Info } from '@material-ui/icons';
import extProps from './propTypes';

const DialogGDPR = ({
  classes, messages, onHideGDPR, isShowGDPR,
}) => {
  const [isShowDialog, setIsShowDialog] = useState(false);

  return (
    <>
      <Snackbar
        className={classes.snackbar}
        anchorOrigin={{
          horizontal: 'center',
          vertical: 'bottom',
        }}
        open={isShowGDPR && !isShowDialog}
        onClose={() => {}}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={(
          <span id="message-id">
            { messages['We use cookies to record...'] }
            <IconButton className={classes.info} onClick={() => setIsShowDialog(true)}>
              <Info />
            </IconButton>
          </span>
        )}
        action={[
          <Button key="undo" color="secondary" size="small" onClick={onHideGDPR}>
            { messages.AGREE }
          </Button>,
        ]}
      />
      <Dialog onClose={() => setIsShowDialog(false)} aria-labelledby="customized-dialog-title" open={isShowDialog}>
        <DialogContent dividers className={classes.dialogContent}>
          <Typography variant="h5">
            { messages['Why do we use cookies?'] }
          </Typography>
          <Typography variant="body1">
            { messages['By visiting our website and using...'] }
          </Typography>
          <Typography variant="body1">
            { messages['Cookies are small amounts of information...'] }
          </Typography>
          <Typography variant="body1">
            { messages['By using Out of bounds, you...'] }
          </Typography>
          <Typography variant="body1">
            <strong>{ messages['Out of bounds(O²B) use cookies for...'] }</strong>
          </Typography>
          <br />
          <Typography variant="body1">
            { messages['To help us recognize your browser...'] }
          </Typography>
          <Typography variant="h5">
            { messages['Your Choices About Cookies and Web Beacons'] }
          </Typography>
          <Typography variant="body1">
            { messages['You have the option of configuring...'] }
          </Typography>
          <Typography variant="body1">
            { messages['You can set your Internet browser...'] }
            {' '}
            <a href="http://www.allaboutcookies.org/fr/">http://www.allaboutcookies.org/fr/</a>
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setIsShowDialog(false)} className={classes.dialogAction}>
            { messages.Close }
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

DialogGDPR.propTypes = extProps;

export default DialogGDPR;
