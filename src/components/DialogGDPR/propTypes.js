import { PropTypes } from 'prop-types';

const propTypes = {
  classes: PropTypes.object.isRequired,
  isShowGDPR: PropTypes.bool.isRequired,
  messages: PropTypes.object.isRequired,
  onHideGDPR: PropTypes.func.isRequired,
};

export default propTypes;
