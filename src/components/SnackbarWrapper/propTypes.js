import { PropTypes } from 'prop-types';
import React from 'react';

export const propTypes = {
  action: PropTypes.node,
  anchorOrigin: PropTypes.shape({
    horizontal: PropTypes.string,
    vertical: PropTypes.string,
  }),
  autoHideDuration: PropTypes.number,
  bgError: PropTypes.bool,
  bgInfo: PropTypes.bool,
  bgSuccess: PropTypes.bool,
  bgWarning: PropTypes.bool,
  classes: PropTypes.object.isRequired,
  customAction: PropTypes.bool,
  isShow: PropTypes.bool.isRequired,
  message: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
};

export const defaultProps = {
  action: (<></>),
  anchorOrigin: {
    horizontal: 'left',
    vertical: 'bottom',
  },
  autoHideDuration: 6000,
  bgError: false,
  bgInfo: false,
  bgSuccess: false,
  bgWarning: false,
  customAction: false,
};

export default propTypes;
