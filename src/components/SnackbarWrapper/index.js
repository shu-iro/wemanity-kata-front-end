import { withStyles } from '@material-ui/core/styles';
import styles from './styles';
import SnackbarWrapper from './SnackbarWrapper';

export default withStyles(styles)(SnackbarWrapper);
