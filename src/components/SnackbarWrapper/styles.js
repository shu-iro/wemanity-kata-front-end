import {
  amber, green, blue, orange, red,
} from '@material-ui/core/colors';

const styles = (theme) => ({
  bgError: {
    '& > button': {
      color: red[900],
    },
    background: red[300],
  },
  bgInfo: {
    '& > button': {
      color: blue[900],
    },
    background: blue[300],
  },
  bgSuccess: {
    '& > button': {
      color: green[900],
    },
    background: green[300],
  },
  bgWarning: {
    '& > button': {
      color: orange[900],
    },
    background: orange[300],
  },
  divider: {
    margin: `${theme.spacing.unit * 3}px 0`,
  },
  error: {
    backgroundColor: theme.palette.error.dark,
  },
  front: {
    zIndex: 1500,
  },
  icon: {
    fontSize: 20,
  },
  iconVariant: {
    marginRight: theme.spacing.unit,
    opacity: 0.9,
  },
  info: {
    backgroundColor: theme.palette.primary.dark,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  message: {
    alignItems: 'center',
    display: 'flex',
  },
  snackbar: {
    margin: theme.spacing.unit,
  },
  success: {
    backgroundColor: green[600],
  },
  warning: {
    backgroundColor: amber[700],
  },
});

export default styles;
