import React from 'react';
import {
  Snackbar, SnackbarContent, IconButton,
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { propTypes as extProps, defaultProps as extDefault } from './propTypes';

const SnackbarWrapper = ({
  classes, anchorOrigin, isShow, autoHideDuration,
  bgInfo, bgSuccess, bgWarning, bgError, message, onClose, customAction, action,
}) => (
  <Snackbar
    open={isShow}
    anchorOrigin={anchorOrigin}
    autoHideDuration={autoHideDuration}
    onClose={onClose}
    className={classes.front}
  >
    <SnackbarContent
      className={`${
        classes.snackbar} ${
        bgInfo ? classes.bgInfo : ''} ${
        bgSuccess ? classes.bgSuccess : ''} ${
        bgWarning ? classes.bgWarning : ''} ${
        bgError ? classes.bgError : ''}`}
      message={message}
      action={customAction
        ? action
        : (
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            className={classes.close}
            onClick={onClose}
          >
            <CloseIcon className={classes.icon} />
          </IconButton>
        )}
    />
  </Snackbar>
);

SnackbarWrapper.propTypes = extProps;

SnackbarWrapper.defaultProps = extDefault;

export default SnackbarWrapper;
