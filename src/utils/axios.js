import { put } from 'redux-saga/effects';
import axios from 'axios';
import stubs from '../stubs';

// Easily create action SUCCESS and ERROR for each async action
export const onSuccess = (action) => `${action}_SUCCESS`;
export const onError = (action) => `${action}_ERROR`;

// Make request based on action and dispatch action
// eslint-disable-next-line func-names
export const callAPI = function* (action) {
  let response = false;

  if (process.env.REACT_APP_STUB_ENABLED === 'true') {
    response = stubs(action.request.method, action.request.url, action.request.data);
    yield new Promise((resolve) => setTimeout(resolve, 500));
  }

  const errorHandling = (error) => {
    if (error.response) {
      return error.response.data.code;
    }
    if (error.request) {
      return 5000;
    }

    return 5000;
  };

  if (!response && action.request.method === 'GET') {
    try {
      const { data } = yield axios.get(action.request.url);
      yield put({
        data: data.data,
        meta: action.request.meta,
        type: onSuccess(action.type),
      });
    } catch (error) {
      yield put({
        error: errorHandling(error),
        meta: action.request.meta,
        type: onError(action.type),
      });
    }
  } else if (!response && action.request.method === 'POST') {
    try {
      const { data } = yield axios.post(action.request.url, action.request.data);
      yield put({
        data: data.data,
        meta: action.request.meta,
        type: onSuccess(action.type),
      });
    } catch (error) {
      yield put({
        error: errorHandling(error),
        meta: action.request.meta,
        type: onError(action.type),
      });
    }
  } else if (!response && action.request.method === 'PUT') {
    try {
      const { data } = yield axios.put(action.request.url, action.request.data);
      yield put({
        data: data.data,
        meta: action.request.meta,
        type: onSuccess(action.type),
      });
    } catch (error) {
      yield put({
        error: errorHandling(error),
        meta: action.request.meta,
        type: onError(action.type),
      });
    }
  } else {
    const { data } = response;
    if (response.error) {
      yield put({
        error: response.error.code,
        meta: action.request.meta,
        type: onError(action.type),
      });
    } else {
      yield put({
        data,
        meta: action.request.meta,
        type: onSuccess(action.type),
      });
    }
  }
};
