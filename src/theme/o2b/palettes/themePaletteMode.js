import lightPalette from './lightPalette';
import darkPalette from './darkPalette';

export default (color, mode) => {
  if (mode === 'dark') {
    return darkPalette[color];
  }

  return lightPalette[color];
};
