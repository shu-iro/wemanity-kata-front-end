export default {
  blueCyanTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#00838F',
        light: '#E0F7FA',
        main: '#00BCD4',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#01579B',
        light: '#E1F5FE',
        main: '#039BE5',
      },
    },
  },
  blueTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#283593',
        light: '#E8EAF6',
        main: '#448AFF',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#0277BD',
        light: '#E1F5FE',
        main: '#03A9F4',
      },
    },
  },
  cyanTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#00695C',
        light: '#E0F7FA',
        main: '#26A69A',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#33691E',
        light: '#F1F8E9',
        main: '#689F38',
      },
    },
  },
  goldTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#FF6D00',
        light: '#FFF9C4',
        main: '#FF9100',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#4E342E',
        light: '#EFEBE9',
        main: '#A1887F',
      },
    },
  },
  greenOrangeTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#33691E',
        light: '#F1F8E9',
        main: '#689F38',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#E65100',
        light: '#FFF8E1',
        main: '#FF8F00',
      },
    },
  },
  greenPurpleTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#311B92',
        light: '#EDE7F6',
        main: '#B388FF',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#1B5E20',
        light: '#E8F5E9',
        main: '#00C853',
      },
    },
  },
  greyTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#455A64',
        light: '#ECEFF1',
        main: '#90A4AE',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#424242',
        light: '#E0E0E0',
        main: '#757575',
      },
    },
  },
  magentaTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#D81B60',
        light: '#FCE4EC',
        main: '#F06292',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#FF6F00',
        light: '#FFF8E1',
        main: '#FFA000',
      },
    },
  },
  orangeTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#E65100',
        light: '#FFF3E0',
        main: '#EF6C00',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#7B1FA2',
        light: '#F3E5F5',
        main: '#BA68C8',
      },
    },
  },
  pinkBlueTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#006064',
        light: '#E0F7FA',
        main: '#00BCD4',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#AD1457',
        light: '#FCE4EC',
        main: '#F06292',
      },
    },
  },
  pinkGreenTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#33691E',
        light: '#DCEDC8',
        main: '#689F38',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#AD1457',
        light: '#FCE4EC',
        main: '#F06292',
      },
    },
  },
  purpleRedTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#512DA8',
        light: '#EDE7F6',
        main: '#B388FF',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#C2185B',
        light: '#FCE4EC',
        main: '#EC407A',
      },
    },
  },
  purpleTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#8E24AA',
        light: '#EDE7F6',
        main: '#BA68C8',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#006064',
        light: '#E0F7FA',
        main: '#00BCD4',
      },
    },
  },
  redTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#E53935',
        light: '#FFEBEE',
        main: '#EF5350',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#455A64',
        light: '#ECEFF1',
        main: '#607D8B',
      },
    },
  },
  skyBlueTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#1565C0',
        light: '#E3F2FD',
        main: '#42A5F5',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#00796B',
        light: '#E0F2F1',
        main: '#00BFA5',
      },
    },
  },
  yellowBlueTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#01579B',
        light: '#E1F5FE',
        main: '#039BE5',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#E65100',
        light: '#FFF3E0',
        main: '#FF9800',
      },
    },
  },
  yellowCyanTheme: {
    palette: {
      primary: {
        contrastText: '#fff',
        dark: '#F57F17',
        light: '#FFF3E0',
        main: '#F9A825',
      },
      secondary: {
        contrastText: '#fff',
        dark: '#006064',
        light: '#E0F7FA',
        main: '#00BCD4',
      },
    },
  },
};
