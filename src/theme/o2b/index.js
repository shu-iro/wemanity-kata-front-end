import themePalette from './palettes/themePaletteMode';

export default (color, mode) => ({
  glow: {
    dark: `0 2px 40px 0px ${themePalette(color, mode).palette.primary.main}`,
    light: `0 2px 20px -5px ${themePalette(color, mode).palette.primary.main}`,
    medium: `0 2px 40px -5px ${themePalette(color, mode).palette.primary.main}`,
  },
  overrides: {
    MuiAppBar: {
      colorPrimary: {
        backgroundColor: mode === 'dark'
          ? themePalette(color, mode).palette.primary.dark
          : themePalette(color, mode).palette.primary.main,
      },
    },
    MuiButton: {
      contained: {
        boxShadow: 'none',
      },
      root: {
        borderRadius: '20px',
        fontWeight: 600,
      },
      sizeSmall: {
        padding: '7px 12px',
      },
    },
    MUIDataTableToolbarSelect: {
      deleteIcon: {
        color: mode === 'dark' ? '#FFF' : '#000',
      },
      root: {
        backgroundColor: mode === 'dark'
          ? themePalette(color, mode).palette.secondary.dark
          : themePalette(color, mode).palette.secondary.light,
        boxShadow: 'none',
      },
    },
    MuiDialogTitle: {
      root: {
        '& h2': {
          color: mode === 'dark'
            ? themePalette(color, mode).palette.primary.light
            : themePalette(color, mode).palette.primary.dark,
        },
        '&:after': {
          background: themePalette(color, mode).palette.primary.main,
          bottom: 0,
          content: '""',
          height: 4,
          left: 26,
          position: 'absolute',
          width: 60,
        },
        marginBottom: 32,
        position: 'relative',
      },
    },
    MuiExpansionPanel: {
      root: {
        '&:first-child': {
          borderTopLeftRadius: 8,
          borderTopRightRadius: 8,
        },
        '&:last-child': {
          borderBottomLeftRadius: 8,
          borderBottomRightRadius: 8,
        },
      },
    },
    MuiFormControl: {
      root: {
        '& label + div': {
          '& input, select, > div > div': {
            padding: '24px 8px 4px',
          },
          '&[role="radiogroup"]': {
            alignItems: 'flex-start',
          },
          alignItems: 'flex-end',
        },
      },
    },
    MuiFormHelperText: {
      root: {
        paddingLeft: 5,
      },
    },
    MuiFormLabel: {
      root: {
        fontSize: 14,
      },
    },
    MuiInput: {
      input: {
        fontSize: 14,
        padding: 10,
      },
      multiline: {
        paddingLeft: 10,
        paddingRight: 10,
        paddingTop: 24,
      },
      root: {
        alignItems: 'center',
        border: mode === 'dark' ? '1px solid rgba(255,255,255,0.32)' : '1px solid rgba(0,0,0,0.32)',
        borderRadius: 6,
        transition: 'border 0.3s ease',
      },
      underline: {
        '&:after': {
          borderRadius: 6,
          bottom: -1,
          boxShadow: `0 0 1px ${themePalette(color, mode).palette.primary.main}`,
          height: 'calc(100% + 1px)',
        },
        '&:before': {
          display: 'none',
        },
      },
    },
    MuiInputAdornment: {
      positionEnd: {
        marginRight: 8,
        marginTop: 32,
      },
      positionStart: {
        marginLeft: 8,
      },
      root: {
        '& button': {
          height: 32,
          padding: 0,
          width: 32,
        },
        '& svg': {
          fontSize: 18,
        },
        alignItems: 'flex-end',
        paddingBottom: 4,
      },
    },
    MuiInputLabel: {
      filled: {
        '&$shrink': {
          transform: 'translate(0px, -6px) scale(0.75)',
        },
        transform: 'translate(2px, 6px) scale(1)',
      },
      formControl: {
        left: 10,
        top: 12,
        transform: 'translate(0, 22px) scale(1)',
      },
      outlined: {
        '&$shrink': {
          transform: 'translate(4px, -16px) scale(0.75)',
        },
        transform: 'translate(2px, 6px) scale(1)',
      },
      shrink: {
        transform: 'translate(0, 13px) scale(0.7)',
        zIndex: 1,
      },
    },
    MuiLinearProgress: {
      bar: {
        borderRadius: 16,
      },
      colorPrimary: {
        backgroundColor: mode === 'dark' ? '#616161' : '#ededed',
      },
      root: {
        borderRadius: 16,
      },
    },
    MuiPaper: {
      elevation1: {
        boxShadow: mode === 'dark'
          ? '0px 1px 3px 0px rgba(64, 64, 64, 1), 0px 1px 1px 0px rgba(42, 42, 42, 1), 0px 2px 1px -1px rgba(20, 20, 20, 1)'
          : '0px 1px 3px 0px rgba(142, 142, 142, 0.2), 0px 1px 1px 0px rgba(243, 243, 243, 0.14), 0px 2px 1px -1px rgba(204, 204, 204, 0.12)',
      },
      elevation4: {
        boxShadow: mode === 'dark'
          ? '0px 2px 4px -1px rgba(64, 64, 64, 0.46), 0px 4px 5px 0px rgba(42, 42, 42, 0.32), 0px 1px 10px 0px rgba(20, 20, 20, 0.12)'
          : '0px 2px 20px 0px rgba(0, 0, 0, 0.15), 0px 4px 5px 0px rgba(243, 243, 243, 0.14), 0px 1px 10px 0px rgba(204, 204, 204, 0.12)',
      },
      rounded: {
        borderRadius: 12,
      },
    },
    MuiSelect: {
      icon: {
        top: 'calc(50% - 10px)',
      },
      root: {
        borderRadius: 6,
      },
    },
    MuiSlider: {
      thumb: {
        boxShadow: '0px 1px 5px 0px rgba(80,80,80, 0.2), 0px 2px 2px 0px rgba(80,80,80, 0.14), 0px 3px 1px -2px rgba(80,80,80, 0.12)',
      },
    },
    MuiSnackbarContent: {
      root: {
        '@media (min-width: 960px)': {
          borderRadius: 32,
        },
      },
    },
    MuiTableCell: {
      head: {
        fontWeight: 600,
      },
      root: {
        borderBottom: mode === 'dark'
          ? '1px solid #636363'
          : `1px solid ${themePalette(color, mode).palette.primary.light}`,
      },
    },
    MuiTablePagination: {
      input: {
        marginLeft: 8,
        marginRight: 32,
      },
      select: {
        paddingRight: 24,
      },
      selectIcon: {
        top: 4,
      },
      selectRoot: {
        marginLeft: 0,
        marginRight: 0,
      },
    },
    MuiTabs: {
      indicator: {
        borderRadius: '10px 10px 0 0',
        height: 4,
      },
      root: {
        borderRadius: 10,
      },
    },
    MuiToggleButton: {
      root: {
        boxShadow: 'none !important',
      },
    },
    MuiToggleButtonGroup: {
      root: {
        border: `1px solid ${themePalette(color, mode).palette.secondary.main}`,
        borderRadius: 20,
        boxShadow: 'none !important',
      },
    },
    MuiToolbar: {
      root: {
        borderRadius: 8,
      },
    },
    MuiTypography: {
      button: {
        fontWeight: 600,
      },
    },
  },
  palette: {
    action: {
      hover: mode === 'dark' ? 'rgba(80,80,80, 0.9)' : 'rgba(80,80,80, 0.05)',
      hoverOpacity: 0.05,
    },
    primary: themePalette(color, mode).palette.primary,
    secondary: themePalette(color, mode).palette.secondary,
    type: mode,
  },
  rounded: {
    big: '20px',
    medium: '12px',
    small: '8px',
  },
  shade: {
    light: '0 10px 15px -5px rgba(62, 57, 107, .07)',
  },
  shadows: mode === 'dark'
    ? [
      'none',
      '0px 1px 3px 0px rgba(50,50,50, 0.2),0px 1px 1px 0px rgba(50,50,50, 0.14),0px 2px 1px -1px rgba(50,50,50, 0.12)',
      '0px 1px 5px 0px rgba(50,50,50, 0.2),0px 2px 2px 0px rgba(50,50,50, 0.14),0px 3px 1px -2px rgba(50,50,50, 0.12)',
      '0px 1px 8px 0px rgba(50,50,50, 0.2),0px 3px 4px 0px rgba(50,50,50, 0.14),0px 3px 3px -2px rgba(50,50,50, 0.12)',
      '0px 2px 4px -1px rgba(50,50,50, 0.2),0px 4px 5px 0px rgba(50,50,50, 0.14),0px 1px 10px 0px rgba(50,50,50, 0.12)',
      '0px 3px 5px -1px rgba(50,50,50, 0.2),0px 5px 8px 0px rgba(50,50,50, 0.14),0px 1px 14px 0px rgba(50,50,50, 0.12)',
      '0px 3px 5px -1px rgba(50,50,50, 0.2),0px 6px 10px 0px rgba(50,50,50, 0.14),0px 1px 18px 0px rgba(50,50,50, 0.12)',
      '0px 4px 5px -2px rgba(50,50,50, 0.2),0px 7px 10px 1px rgba(50,50,50, 0.14),0px 2px 16px 1px rgba(50,50,50, 0.12)',
      '0px 5px 5px -3px rgba(50,50,50, 0.2),0px 8px 10px 1px rgba(50,50,50, 0.14),0px 3px 14px 2px rgba(50,50,50, 0.12)',
      '0px 5px 6px -3px rgba(50,50,50, 0.2),0px 9px 12px 1px rgba(50,50,50, 0.14),0px 3px 16px 2px rgba(50,50,50, 0.12)',
      '0px 6px 6px -3px rgba(50,50,50, 0.2),0px 10px 14px 1px rgba(50,50,50, 0.14),0px 4px 18px 3px rgba(50,50,50, 0.12)',
      '0px 6px 7px -4px rgba(50,50,50, 0.2),0px 11px 15px 1px rgba(50,50,50, 0.14),0px 4px 20px 3px rgba(50,50,50, 0.12)',
      '0px 7px 8px -4px rgba(50,50,50, 0.2),0px 12px 17px 2px rgba(50,50,50, 0.14),0px 5px 22px 4px rgba(50,50,50, 0.12)',
      '0px 7px 8px -4px rgba(50,50,50, 0.2),0px 13px 19px 2px rgba(50,50,50, 0.14),0px 5px 24px 4px rgba(50,50,50, 0.12)',
      '0px 7px 9px -4px rgba(50,50,50, 0.2),0px 14px 21px 2px rgba(50,50,50, 0.14),0px 5px 26px 4px rgba(50,50,50, 0.12)',
      '0px 8px 9px -5px rgba(50,50,50, 0.2),0px 15px 22px 2px rgba(50,50,50, 0.14),0px 6px 28px 5px rgba(50,50,50, 0.12)',
      '0px 8px 10px -5px rgba(50,50,50, 0.2),0px 16px 24px 2px rgba(50,50,50, 0.14),0px 6px 30px 5px rgba(50,50,50, 0.12)',
      '0px 8px 11px -5px rgba(50,50,50, 0.2),0px 17px 26px 2px rgba(50,50,50, 0.14),0px 6px 32px 5px rgba(50,50,50, 0.12)',
      '0px 9px 11px -5px rgba(50,50,50, 0.2),0px 18px 28px 2px rgba(50,50,50, 0.14),0px 7px 34px 6px rgba(50,50,50, 0.12)',
      '0px 9px 12px -6px rgba(50,50,50, 0.2),0px 19px 29px 2px rgba(50,50,50, 0.14),0px 7px 36px 6px rgba(50,50,50, 0.12)',
      '0px 10px 13px -6px rgba(50,50,50, 0.2),0px 20px 31px 3px rgba(50,50,50, 0.14),0px 8px 38px 7px rgba(50,50,50, 0.12)',
      '0px 10px 13px -6px rgba(50,50,50, 0.2),0px 21px 33px 3px rgba(50,50,50, 0.14),0px 8px 40px 7px rgba(50,50,50, 0.12)',
      '0px 10px 14px -6px rgba(50,50,50, 0.2),0px 22px 35px 3px rgba(50,50,50, 0.14),0px 8px 42px 7px rgba(50,50,50, 0.12)',
      '0px 11px 14px -7px rgba(50,50,50, 0.2),0px 23px 36px 3px rgba(50,50,50, 0.14),0px 9px 44px 8px rgba(50,50,50, 0.12)',
      '0px 11px 15px -7px rgba(50,50,50, 0.2),0px 24px 38px 3px rgba(850,50,50 0.14),0px 9px 46px 8px rgba(50,50,50, 0.12)',
    ]
    : [
      'none',
      '0px 1px 3px 0px rgba(80,80,80, 0.2),0px 1px 1px 0px rgba(80,80,80, 0.14),0px 2px 1px -1px rgba(80,80,80, 0.12)',
      '0px 1px 5px 0px rgba(80,80,80, 0.2),0px 2px 2px 0px rgba(80,80,80, 0.14),0px 3px 1px -2px rgba(80,80,80, 0.12)',
      '0px 1px 8px 0px rgba(80,80,80, 0.2),0px 3px 4px 0px rgba(80,80,80, 0.14),0px 3px 3px -2px rgba(80,80,80, 0.12)',
      '0px 2px 4px -1px rgba(80,80,80, 0.2),0px 4px 5px 0px rgba(80,80,80, 0.14),0px 1px 10px 0px rgba(80,80,80, 0.12)',
      '0px 3px 5px -1px rgba(80,80,80, 0.2),0px 5px 8px 0px rgba(80,80,80, 0.14),0px 1px 14px 0px rgba(80,80,80, 0.12)',
      '0px 3px 5px -1px rgba(80,80,80, 0.2),0px 6px 10px 0px rgba(80,80,80, 0.14),0px 1px 18px 0px rgba(80,80,80, 0.12)',
      '0px 4px 5px -2px rgba(80,80,80, 0.2),0px 7px 10px 1px rgba(80,80,80, 0.14),0px 2px 16px 1px rgba(80,80,80, 0.12)',
      '0px 5px 5px -3px rgba(80,80,80, 0.2),0px 8px 10px 1px rgba(80,80,80, 0.14),0px 3px 14px 2px rgba(80,80,80, 0.12)',
      '0px 5px 6px -3px rgba(80,80,80, 0.2),0px 9px 12px 1px rgba(80,80,80, 0.14),0px 3px 16px 2px rgba(80,80,80, 0.12)',
      '0px 6px 6px -3px rgba(80,80,80, 0.2),0px 10px 14px 1px rgba(80,80,80, 0.14),0px 4px 18px 3px rgba(80,80,80, 0.12)',
      '0px 6px 7px -4px rgba(80,80,80, 0.2),0px 11px 15px 1px rgba(80,80,80, 0.14),0px 4px 20px 3px rgba(80,80,80, 0.12)',
      '0px 7px 8px -4px rgba(80,80,80, 0.2),0px 12px 17px 2px rgba(80,80,80, 0.14),0px 5px 22px 4px rgba(80,80,80, 0.12)',
      '0px 7px 8px -4px rgba(80,80,80, 0.2),0px 13px 19px 2px rgba(80,80,80, 0.14),0px 5px 24px 4px rgba(80,80,80, 0.12)',
      '0px 7px 9px -4px rgba(80,80,80, 0.2),0px 14px 21px 2px rgba(80,80,80, 0.14),0px 5px 26px 4px rgba(80,80,80, 0.12)',
      '0px 8px 9px -5px rgba(80,80,80, 0.2),0px 15px 22px 2px rgba(80,80,80, 0.14),0px 6px 28px 5px rgba(80,80,80, 0.12)',
      '0px 8px 10px -5px rgba(80,80,80, 0.2),0px 16px 24px 2px rgba(80,80,80, 0.14),0px 6px 30px 5px rgba(80,80,80, 0.12)',
      '0px 8px 11px -5px rgba(80,80,80, 0.2),0px 17px 26px 2px rgba(80,80,80, 0.14),0px 6px 32px 5px rgba(80,80,80, 0.12)',
      '0px 9px 11px -5px rgba(80,80,80, 0.2),0px 18px 28px 2px rgba(80,80,80, 0.14),0px 7px 34px 6px rgba(80,80,80, 0.12)',
      '0px 9px 12px -6px rgba(80,80,80, 0.2),0px 19px 29px 2px rgba(80,80,80, 0.14),0px 7px 36px 6px rgba(80,80,80, 0.12)',
      '0px 10px 13px -6px rgba(80,80,80, 0.2),0px 20px 31px 3px rgba(80,80,80, 0.14),0px 8px 38px 7px rgba(80,80,80, 0.12)',
      '0px 10px 13px -6px rgba(80,80,80, 0.2),0px 21px 33px 3px rgba(80,80,80, 0.14),0px 8px 40px 7px rgba(80,80,80, 0.12)',
      '0px 10px 14px -6px rgba(80,80,80, 0.2),0px 22px 35px 3px rgba(80,80,80, 0.14),0px 8px 42px 7px rgba(80,80,80, 0.12)',
      '0px 11px 14px -7px rgba(80,80,80, 0.2),0px 23px 36px 3px rgba(80,80,80, 0.14),0px 9px 44px 8px rgba(80,80,80, 0.12)',
      '0px 11px 15px -7px rgba(80,80,80, 0.2),0px 24px 38px 3px rgba(80,80,80, 0.14),0px 9px 46px 8px rgba(80,80,80, 0.12)',
    ],
  typography: {
    body2: {
      fontWeight: 500,
    },
    fontFamily: [
      'Open Sans',
      'sans-serif',
    ].join(','),
    fontWeightMedium: 600,
    title: {
      fontWeight: 600,
    },
    useNextVariants: true,
  },
});
