# **Wemanity kata phonebook challenge.**
## **About**
This is a production ready architecture, using  the following technologies:
- **express**: Small production webserver.
- **material-ui**: React components for faster and easier web development.
- **animate.css**: CSS basic animation.
- **eslint**: Code quality.
- **node-sass**: Node.js bindings to libsass. 
- **react**: Last version of React.
- **react-router-dom**: DOM binding for react-router .
- **react-scripts**: Create React apps with no build configuration. 
- **redux**: A predictable state container for JavaScript apps.
- **redux-persist**: Persist and rehydrate a redux store.
- **redux-saga**: A library that aims to make application side effects easier to manage.
- **testcafe**: End-to-end testing

## **Installation**
`npm install`
## **Development**
`npm run start:dev`
or with a fake API
`npm run start:dev-stubs`
## **Production**
```
npm run build
npm run start:production
```
## **Test**
Check code quality with eslint `npm run test:eslint`

Run unit test(not implemented) `npm run test:unit`

Run end-to-end test(work with a fake API) `npm run test:e2e`

Eslint + e2e + unit `npm run test`
(If more time add unit test)
## **Deploy**
I have implemented a complete CI/CD based on Giltab CI/CD.
Automatically deploy in CapRover with help of Docker.
## **Architecture summary**
- **/e2e** - All end-to-end testcafe scenario
- **/public** - index.html and favicon
- **/server** - Small webserver for production
- **/src**
    - **/components** - Contains all components that can be used in different containers or applications
    - **/constants** - All constants (endpoint, error,...)
    - **/containers** - Contains all the "smart" components(often close to the notion of page)
    - **/images** - Static images
    - **/redux**
        - **/config** - Configuration of store persistence, middleware ...
        - **/features** - Your reducer, action and side effect separate by object manipulate
    - **/stubs** - A fake API used for dev and end-to-end testing
    - **/theme** - Material-UI theme
    - **/translations** - Translation with key = english version and value translated version
    - **/types** - PropsTypes often used in your application
    - **/utils**
        - **/axios.js** - Handle all request to API and automatically switch to stubs if env set
