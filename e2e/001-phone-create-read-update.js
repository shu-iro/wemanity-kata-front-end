/* eslint-disable newline-per-chained-call */
import { waitForReact, ReactSelector } from 'testcafe-react-selectors';
import english from '../src/translations/english';

// eslint-disable-next-line no-unused-expressions
fixture`Phone create/read/update`
  .page`${process.env.REACT_APP_TESTING_URL}`
  .beforeEach(async () => {
    await waitForReact();
  });

test('Check initial state, accept GDPR', async (t) => {
  const phoneBookeEmpty = ReactSelector('PhoneBookRead').find('h3').withText('PhoneBook is empty.');

  const buttonCount = ReactSelector('PhoneBook').find('button').count;
  const cardTitle = ReactSelector('PhoneBook').find('span').withText('Manage your PhoneBook');
  const cardSubTitle = ReactSelector('PhoneBook').find('span').withText('Wemanity kata');

  const snackbarGDPRConsent = ReactSelector('DialogGDPR').find('span').withText(english['We use cookies to record...']);
  const buttonAcceptGDPR = ReactSelector('DialogGDPR').find('button').nth(1);

  await t
    .expect(phoneBookeEmpty.exists).ok()
    .expect(cardTitle.exists).ok()
    .expect(cardSubTitle.exists).ok()
    .expect(snackbarGDPRConsent.exists).ok()
    .expect(buttonCount).eql(5)
    .click(buttonAcceptGDPR)
    .expect(buttonCount).eql(3);
});

test('Create two new phone number, update first and use search bar', async (t) => {
  const buttonAddPhone = ReactSelector('PhoneBook').find('button').nth(2);
  // After the creation of first number two new button is added before add button
  // (card with copy and edit button)
  const buttonAddPhone2 = ReactSelector('PhoneBook').find('button').nth(4);

  const titleCreate = ReactSelector('PhoneCardForm').find('span').withText('Create an entry');
  const inputLastName = ReactSelector('PhoneCardForm').find('input').nth(0);
  const inputFirstName = ReactSelector('PhoneCardForm').find('input').nth(1);
  const inputPhoneNumber = ReactSelector('PhoneCardForm').find('input').nth(2);
  const buttonAddConfirm = ReactSelector('PhoneCardForm').find('button').nth(1);

  const typoName1 = ReactSelector('PhoneCard').find('span').withText('Vanmoortel Nolan');
  const typoPhone1 = ReactSelector('PhoneCard').find('span').withText('+32 47 0920218');
  const typoName2 = ReactSelector('PhoneCard').find('span').withText('Dupont Jean');
  const typoPhone2 = ReactSelector('PhoneCard').find('span').withText('+36 45 1234567');

  // Create two phone number
  await t
    .click(buttonAddPhone)
    .wait(200)
    .expect(titleCreate.exists).ok()
    .click(buttonAddConfirm)
    .typeText(inputLastName, 'Vanmoortel')
    .typeText(inputFirstName, 'Nolan')
    .typeText(inputPhoneNumber, '+32470920218')
    .click(buttonAddConfirm)
    .click(inputPhoneNumber)
    .pressKey('ctrl+a delete')
    .typeText(inputPhoneNumber, '+32 47 0920218')
    .click(buttonAddConfirm)
    .expect(typoName1.exists).ok()
    .expect(typoPhone1.exists).ok()
    .wait(500)
    // Create second
    .click(buttonAddPhone2)
    .wait(200)
    .expect(titleCreate.exists).ok()
    .typeText(inputLastName, 'Dupont')
    .typeText(inputFirstName, 'Jean')
    .typeText(inputPhoneNumber, '+36 45 1234567')
    .click(buttonAddConfirm)
    .expect(typoName1.exists).ok()
    .expect(typoPhone1.exists).ok()
    .expect(typoName2.exists).ok()
    .expect(typoPhone2.exists).ok();

  const buttonEditPhone = ReactSelector('PhoneBook').find('button').nth(2);

  const titleEdit = ReactSelector('PhoneCardForm').find('span').withText('Update an entry');
  const inputLastNameEdit = ReactSelector('PhoneCardForm').find('input').withAttribute('value', 'Vanmoortel');
  const inputFirstNameEdit = ReactSelector('PhoneCardForm').find('input').withAttribute('value', 'Nolan');
  const inputPhoneNumberEdit = ReactSelector('PhoneCardForm').find('input').withAttribute('value', '+32 47 0920218');
  const buttonEditConfirm = ReactSelector('PhoneCardForm').find('button').nth(1);

  const typoName1Edit = ReactSelector('PhoneCard').find('span').withText('Vanmoortel2 Nolan2');
  const typoPhone1Edit = ReactSelector('PhoneCard').find('span').withText('+32 47 09202182');

  // Update first
  await t
    .click(buttonEditPhone)
    .wait(200)
    .expect(titleEdit.exists).ok()
    .typeText(inputLastNameEdit, '2')
    .typeText(inputFirstNameEdit, '2')
    .typeText(inputPhoneNumberEdit, '2')
    .click(buttonEditConfirm)
    .expect(typoName1Edit.exists).ok()
    .expect(typoPhone1Edit.exists).ok();

  const buttonSearch = ReactSelector('PhoneBook').find('button').nth(5);
  const inputSearch = ReactSelector('PhoneBook').find('input');

  // Search Jean
  await t
    .click(buttonSearch)
    .wait(50)
    .typeText(inputSearch, 'J')
    .wait(50)
    .expect(typoName1Edit.exists).notOk()
    .expect(typoName2).ok()
    .typeText(inputSearch, 'Jk')
    .wait(50)
    .expect(typoName1Edit.exists).notOk()
    .expect(typoName2.exists).notOk()
    .click(inputSearch)
    .pressKey('ctrl+a delete')
    .wait(50)
    .expect(typoName1Edit.exists).ok()
    .expect(typoName2.exists).ok()
    .typeText(inputSearch, 'eAn')
    .wait(50)
    .expect(typoName1Edit.exists).notOk()
    .expect(typoName2.exists).ok();
});
